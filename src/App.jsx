import { useEffect, useState } from "react";
import "./App.css";
import CustomerForm from "./Pages/Form";
import { Router, useRoutes } from "react-router-dom";
import TopBar from "./components/Topbar";
import Home from "./Pages/Home.jsx";
import ApiStore from "./apis/ApiStore";
import ErrorPage from "./Pages/ErrorPage";

function App() {
  const routes = useRoutes([
    {
      path: "/create",
      element: <CustomerForm />,
    },
    {
      path: "/home",
      element: <Home />,
    },
    {
      path: "*",
      element: <ErrorPage />,
    },
  ]);
  return (
    <>
      <TopBar />
      {routes}
    </>
  );
}

export default App;
