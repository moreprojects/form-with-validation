import axios from "./ApiDefaults"

export default {
  getCustomers: function () {
    return axios.get(`customers/all`);
  },
  createCustomerEntry: function (data) {
    console.log("%cThis is data %o","background-color: green; color: white",data);

    return axios.post(
      `customers/customer`,{
        data:data,
      }
    );
  },
  deleteCustomer: function (customerId) {
    return axios.delete(
      `customers/${customerId}`
    );
  },
  updateCustomer: function (customerId, data) {
    return axios.put(
      `customers/${customerId}`,
      {data: data}
    );
  },
};