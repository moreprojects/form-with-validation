import AccordionComp from "../components/Accordion";
import "bootstrap/dist/css/bootstrap.css";
import Accordion from "react-bootstrap/Accordion";
import AddressSection from "../components/AddressSection";
import PersonalDetails from "../components/PersonalDetails";
import { useState } from "react";
import ApiStore from "../apis/ApiStore";

export default function CustomerForm() {
  const [data, setData] = useState({});
  const [accordionKey, setAccordionKey] = useState("personal");
  function handleSubmission() {
    ApiStore.createCustomerEntry(data)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }

  return (
    <Accordion activeKey={accordionKey}>
      <AccordionComp
        name="PersonalDetails"
        eventKey="personal"
        setKey={setAccordionKey}
      >
        <PersonalDetails
          setKey={setAccordionKey}
          setData={setData}
          data={data}
        />
      </AccordionComp>
      <AccordionComp name="Address" eventKey="address" setKey={setAccordionKey}>
        <AddressSection
          setData={setData}
          data={data}
          submitForm={handleSubmission}
        />
      </AccordionComp>
    </Accordion>
  );
}
