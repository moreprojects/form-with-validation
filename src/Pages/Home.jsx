import { useEffect, useState } from "react";
import FormTable from "../components/Table";
import ApiStore from "../apis/ApiStore";

export default function Home(props) {
  const [data, setData] = useState({});

  useEffect(() => {
    ApiStore.getCustomers()
      .then((response) => {
        let modifiedData = response.data.reduce((accu, val) => {
          // console.log(val);
          accu[val.id] = val;
          delete accu[val.id].id;
          return accu;
        }, {});
        console.log(modifiedData);
        setData(modifiedData);
      })
      .catch((error) => {
        console.log(error.message);
      });
    // if (data.length > 0) console.log(data);
    return () => setData([]);
  }, []);

  return (
    <>
      {Object.keys(data).length > 0 && (
        <FormTable data={data} setData={setData} />
      )}
    </>
  );
}
