import Accordion from "react-bootstrap/Accordion";

function AccordionComp({ setKey, eventKey, name, children }) {
  const handleOnClick = (event) => {
    setKey(event.target.eventKey);
  };
  return (
    <Accordion.Item eventKey={eventKey}>
      <Accordion.Header onClick={handleOnClick}>{name}</Accordion.Header>
      <Accordion.Body>{children}</Accordion.Body>
    </Accordion.Item>
  );
}

export default AccordionComp;
