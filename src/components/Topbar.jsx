import { Link, Outlet } from "react-router-dom";
function TopBar() {
  return (
    <div
      style={{
        padding: "10px",
        width: "100%",
        height: "3rem",
        backgroundColor: "lightblue",
      }}
    >
      <Link to="/home">
        {" "}
        <img
          src="src/assets/list.svg"
          alt="Show Data"
          height={"30px"}
          width={"30px"}
        />
      </Link>
      <Link to="/create">
        {" "}
        <img
          src="src/assets/ui-add.svg"
          alt="Create Form"
          height={"30px"}
          width={"30px"}
        />
      </Link>
      {/* <Outlet context={{ data: data }} /> */}
    </div>
  );
}

export default TopBar;
