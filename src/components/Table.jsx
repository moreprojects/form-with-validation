import { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import ApiStore from "../apis/ApiStore";

export default function FormTable({ data, setData }) {
  const [updatedData, setUpdatedData] = useState(data);
  const [updatedId, setUpdatedId] = useState(data[Object.keys(data)[0]]);

  let tableHeaderData = Object.keys(data[Object.keys(data)[0]]);

  useEffect(() => {
    ApiStore.getCustomers()
      .then((response) => {
        let modifiedData = response.data.reduce((accu, val) => {
          accu[val.id] = val;
          delete accu[val.id].id;
          return accu;
        }, {});
        console.log(modifiedData);
        setUpdatedData(modifiedData);
      })
      .catch((error) => {
        console.log(error.message);
      });

    return () => setUpdatedData(data);
  }, [updatedId]);

  const updateHandler = (event, id) => {
    let columnName = event.target.className.split(" ")[0];
    console.log(id + " : " + columnName + " " + event.target.textContent);
    console.log();
    ApiStore.updateCustomer(id, {
      [columnName]: event.target.textContent,
    })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  const deleteHandler = (event) => {
    ApiStore.deleteCustomer(event.target.className)
      .then((response) => {
        setUpdatedId(event.target.className);
      })
      .catch((error) => {
        console.log(error.message);
      });
  };

  // console.log(data);
  return (
    <Table striped bordered hover responsive>
      {tableHeaderData && (
        <>
          <thead>
            <tr>
              {tableHeaderData.map((headData) => {
                return <th key={headData}>{headData}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {Object.keys(updatedData).map((keyId) => {
              return (
                <tr key={keyId}>
                  {Object.keys(updatedData[keyId]).map((entry) => {
                    // console.log(entry + " : " + updatedData[keyId][entry]);

                    return (
                      <td
                        key={entry + " " + updatedData[keyId][entry]}
                        className={entry + " " + updatedData[keyId][entry]}
                        contentEditable
                        onBlur={(event) => {
                          // console.log(event);
                          updateHandler(event, keyId);
                        }}
                        // onInput={(event) => {
                        //   console.log(event);
                        //   updateHandler(event, val.id, "firstName");
                        // }}
                      >
                        {updatedData[keyId][entry]}
                      </td>
                    );
                  })}
                  <td>
                    <img
                      src="src/assets/ui-delete.svg"
                      alt="Show Data"
                      height={"20px"}
                      width={"20px"}
                      className={keyId}
                      onClick={deleteHandler}
                      style={{ cursor: "pointer" }}
                    />
                  </td>
                </tr>
              );

              //   console.log(keyId);
            })}
          </tbody>
        </>
      )}
    </Table>
  );
}
