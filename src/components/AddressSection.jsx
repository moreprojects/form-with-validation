import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import * as formik from "formik";
import * as yup from "yup";
import { useState } from "react";
import ToastComp from "./Toast";

export default function AddressSection({ data, setData, submitForm }) {
  const { Formik } = formik;
  const [toast, setToast] = useState(false);

  const schema = yup.object().shape({
    doorNo: yup.string().required(),
    street: yup.string().required(),
    city: yup.string().required(),
    state: yup.string().required(),
    zip: yup.string().required(),
    phone: yup
      .string()
      .matches("[6-9][0-9]{9}", "Must be a valid Mobile number")
      .required(),
    alternatePhone: yup
      .string()
      .matches("[6-9][0-9]{9}", "Must be a valid Mobile number")
      .required(),
    email: yup.string().required().email("Email is not valid"),
  });

  async function handleSubmitButton(values) {
    try {
      console.log(values);
      await setData((Data) => {
        return { ...Data, ...values };
      });
      await submitForm();
      setToast(true);
      setTimeout(() => {
        setToast(false);
      }, 2000);
    } catch (error) {
      console.log(error.message);
    }
  }

  return (
    <>
      <Formik
        validationSchema={schema}
        onSubmit={(values) => handleSubmitButton(values)}
        initialValues={{
          phone: "",
          alternatePhone: "",
          email: "",
          doorNo: "",
          street: "",
          city: "",
          state: "",
          zip: "",
        }}
      >
        {({ handleSubmit, handleChange, values, touched, errors }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <h2>Contact Details</h2>
            <Row className="mb-3">
              <Form.Group
                as={Col}
                md="6"
                controlId="validationFormik106"
                className="position-relative mb-3"
              >
                <Form.Label>Phone</Form.Label>
                <Form.Control
                  type="tel"
                  name="phone"
                  value={values.phone}
                  onChange={handleChange}
                  isInvalid={touched.phone && !!errors.phone}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.phone}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group
                as={Col}
                md="6"
                controlId="validationFormik107"
                className="position-relative"
              >
                <Form.Label>Alternate Phone</Form.Label>
                <Form.Control
                  type="tel"
                  name="alternatePhone"
                  value={values.alternatePhone}
                  onChange={handleChange}
                  isInvalid={touched.alternatePhone && !!errors.alternatePhone}
                />

                <Form.Control.Feedback type="invalid">
                  {errors.alternatePhone}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group
                as={Col}
                md="6"
                controlId="validationFormik108"
                className="position-relative"
              >
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  isInvalid={touched.email && !!errors.email}
                />

                <Form.Control.Feedback type="invalid">
                  {errors.email}
                </Form.Control.Feedback>
              </Form.Group>
            </Row>
            <h3>Temporary Address</h3>
            <Row className="mb-3">
              <Form.Group
                as={Col}
                md="6"
                controlId="validationFormik109"
                className="position-relative"
              >
                <Form.Label>Door No</Form.Label>
                <Form.Control
                  type="text"
                  name="doorNo"
                  value={values.doorNo}
                  onChange={handleChange}
                  isInvalid={touched.doorNo && !!errors.doorNo}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.doorNo}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group
                as={Col}
                md="6"
                controlId="validationFormik110"
                className="position-relative"
              >
                <Form.Label>street</Form.Label>
                <Form.Control
                  type="text"
                  name="street"
                  value={values.street}
                  onChange={handleChange}
                  isInvalid={touched.street && !!errors.street}
                />

                <Form.Control.Feedback type="invalid">
                  {errors.street}
                </Form.Control.Feedback>
              </Form.Group>
            </Row>
            <Row className="mb-3">
              <Form.Group
                as={Col}
                md="6"
                controlId="validationFormik111"
                className="position-relative"
              >
                <Form.Label>City</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="City"
                  name="city"
                  value={values.city}
                  onChange={handleChange}
                  isInvalid={touched.city && !!errors.city}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.city}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group
                as={Col}
                md="3"
                controlId="validationFormik112"
                className="position-relative"
              >
                <Form.Label>State</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="State"
                  name="state"
                  value={values.state}
                  onChange={handleChange}
                  isInvalid={touched.state && !!errors.state}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.state}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group
                as={Col}
                md="3"
                controlId="validationFormik113"
                className="position-relative"
              >
                <Form.Label>Zip</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Zip"
                  name="zip"
                  value={values.zip}
                  onChange={handleChange}
                  isInvalid={touched.zip && !!errors.zip}
                />

                <Form.Control.Feedback type="invalid">
                  {errors.zip}
                </Form.Control.Feedback>
              </Form.Group>
            </Row>

            <Button type="submit">Submit form</Button>
          </Form>
        )}
      </Formik>
      {toast && ToastComp}
    </>
  );
}
