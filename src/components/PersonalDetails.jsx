import Button from "react-bootstrap/Button";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import * as formik from "formik";
import * as yup from "yup";
import dayjs from "dayjs";

export default function PersonalDetails({ setKey, data, setData }) {
  const { Formik } = formik;
  const belowSixteen = dayjs().subtract(16, "year").format("DD-MM-YYYY");
  const schema = yup.object().shape({
    firstName: yup
      .string()
      .matches("^[a-zA-Z]{2,20}$", "First name must have 3-20 alphabets")
      .required("First Name Required"),
    lastName: yup
      .string()
      .matches("^[a-zA-Z]{2,20}$", "Last name must have 3-20 alphabets")
      .required("Last Name Required"),
    dateOfBirth: yup
      .date()
      .max(belowSixteen, "Must be atleast 16 years as of Today")
      .required("Must provide date Of Birth"),
    age: yup
      .number("Age Must Be Number")

      .required("Enter Your Age")
      .positive("Must be positive number")
      .min(16, "Must be greater than 16 ")
      .max(80, "Must be lessthan 80")
      .integer("Must be non-decimal"),
    profession: yup
      .string()
      .matches("^[a-zA-Z]{2,20}$", "Profession must have 3-20 alphabets")
      .required(),
  });
  function handleNextButton(values) {
    setData((Data) => {
      return { ...Data, ...values };
    });
    setKey("address");
  }
  return (
    <Formik
      validationSchema={schema}
      onSubmit={handleNextButton}
      initialValues={{
        firstName: "",
        lastName: "",
        dateOfBirth: "",
        age: "",
        profession: "",
      }}
      validateOnChange
    >
      {({ handleSubmit, handleChange, values, touched, errors }) => (
        <Form noValidate onSubmit={handleSubmit}>
          <Row className="mb-3">
            <Form.Group
              as={Col}
              md="6"
              controlId="validationFormik101"
              className="position-relative"
            >
              <Form.Label>First name</Form.Label>
              <Form.Control
                type="text"
                name="firstName"
                value={values.firstName}
                onChange={handleChange}
                isValid={touched.firstName && !errors.firstName}
                isInvalid={touched.firstName && !!errors.firstName}
              />
              <Form.Control.Feedback type="invalid">
                {errors.firstName}
              </Form.Control.Feedback>
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              as={Col}
              md="6"
              controlId="validationFormik102"
              className="position-relative"
            >
              <Form.Label>Last name</Form.Label>
              <Form.Control
                type="text"
                name="lastName"
                value={values.lastName}
                onChange={handleChange}
                isValid={touched.lastName && !errors.lastName}
                isInvalid={touched.lastName && !!errors.lastName}
              />
              <Form.Control.Feedback type="invalid">
                {errors.lastName}
              </Form.Control.Feedback>
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
          </Row>
          <Row className="mb-3">
            <Form.Group
              as={Col}
              md="6"
              controlId="validationFormik103"
              className="position-relative"
            >
              <Form.Label>Date Of Birth</Form.Label>
              <Form.Control
                type="date"
                name="dateOfBirth"
                value={values.dateOfBirth}
                onChange={handleChange}
                isInvalid={touched.dateOfBirth && !!errors.dateOfBirth}
              />
              <Form.Control.Feedback type="invalid" tooltip>
                {errors.dateOfBirth}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              as={Col}
              md="3"
              controlId="validationFormik104"
              className="position-relative"
            >
              <Form.Label>Age</Form.Label>
              <Form.Control
                type="number"
                placeholder="age"
                name="age"
                value={values.age}
                onChange={handleChange}
                isValid={touched.age && !errors.age}
                isInvalid={touched.age && !!errors.age}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid" tooltip>
                {errors.age}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              as={Col}
              md="3"
              controlId="validationFormik105"
              className="position-relative"
            >
              <Form.Label>Profession</Form.Label>
              <Form.Control
                type="text"
                name="profession"
                value={values.profession}
                onChange={handleChange}
                isInvalid={touched.profession && !!errors.profession}
              />

              <Form.Control.Feedback type="invalid" tooltip>
                {errors.profession}
              </Form.Control.Feedback>
            </Form.Group>
          </Row>
          <Button ref={btnref} className="PersonalBtn" type="submit">
            Next
          </Button>
        </Form>
      )}
    </Formik>
  );
}
